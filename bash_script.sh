# update the repositories 
sudo apt update

# install ngixn
sudo apt install nginx

# start nginx
sudo systemctl start nginx

# Move the file into the correct location
sudo mv ~/kofi_website/index.html /var/www/html/index.html

# restart nginx
sudo systemctl restart nginx